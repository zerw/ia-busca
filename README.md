# README #

Implementar pelo menos um algoritmo de busca de cada uma das categorias a seguir:

* busca cega;
* busca informada.

Entrada de dados conforme o arquivo “Entrada.txt”:

	início(a).			% estado inicial
	final(d).			% estado final
	caminho(a,b,100). 	% existe um caminho entre “a” e “b” de comprimento 100
	caminho(b,c,20).
	caminho(a,d,150).
	caminho(a,c,200).
	h(a,d,2).			% heurística entre “a” e “d” com valor de 2
	h(b,d,20).
	h(c,d,10).

O algoritmo deve guardar:

1. Caminho de Início até Final, identificados nas duas primeiras linhas do arquivo de entrada de dados;
2. Alguma forma de quantificar o desempenho do algoritmo. Justificar a escolha desse modo de quantificação.
