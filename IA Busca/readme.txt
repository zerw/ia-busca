Readme

Para a execução da aplicação insira no arquivo "entrada.txt" o espaço de busca desejado. 
São fornecidos três arquivos de espaços de busca exemplos: 
	"entrada 01.txt" ==> "EB1.png"
	"entrada 02.txt" ==> "EB2.png"
	"entrada 03.txt" ==> "EB3.png"
Caso deseja-se utilizar um espaço de busca diferente, utilize o mesmo padrão dos arquivos fornecidos. 
Nota-se que é necessário utilizar o padrão dos dados, não sendo necessário estarem na mesma ordem, contudo,
caso seja necessário manter a ordem das ramificações dos nós filhos, os nós mais a esquerda devem ser os
primeiros a aparecerem no arquivo de entrada. 
Além da sintaxe de cada dado, é possível fornecer apenas um nó inicial.

Após a construção do arquivo de entrada, coloque o arquivo no mesmo diretório do executavel "IA Busca.exe"
e rode o executavel. 
Após isso, serão gerados os arquivos "readGraph.log" e "report.chris". 
O primeiro apresenta um log da leitura do arquivo de entrada, sempre adicionando o log ao final fo arquivo.
No log são fornecidas as seguintes informações: 
	Data e hora da execução
	Total de raizes (1 ou 0) 
	Nós finais
	Caminhos entre nós
	Heurísticas
	Avisos
	Erros 
Todos os avisos e erros são inseridos após a data. 
O arquivo "report.chris" apresenta um relatório da busca dos algoritmos DFS com e sem backtrack e Best
First com Greedy, sendo este arquivo reescrito após cada execução. 
Para cada algoritmo é exibido: 
	Solução não encontrada ou o caminho da solução encontrada
	Custo de espaço em cada iteração da busca.


