#include <iostream>

using namespace std;

#include <iostream>
#include <string>
#include <list>

#include "Node.h"
#include "Graph.h"
#include "IO.h"
#include "Print.h"
#include "Search.h"

int main(){
	Graph graph;
    std::list<Report> reports;

    graph = IO().readFile();

    // Print().printGraph(graph);

    reports.push_back(Search().depthFirstSearch(graph, true));
    reports.push_back(Search().depthFirstSearch(graph, false));
	reports.push_back(Search().greedySearch(graph));
    if (reports.front().getFoundSolution()){
    	Print().printList(reports.front().getSolutionPath(), "Solution");
    } else{
    	Print().printString("Solution not found for " + reports.front().getAlgorithmName());
    }

    IO().writeReport(reports);

    return 0;
}
