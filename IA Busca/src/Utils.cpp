#include "Utils.h"

Utils::Utils()
{
    //ctor
}

Utils::~Utils()
{
    //dtor
}

std::string Utils::toString(int number){
	std::stringstream ss;
	ss << number;
	return ss.str();
}

int Utils::find(std::list<std::string> myList, std::string str){
	int i = 0;
	for (std::list<std::string>::iterator it=myList.begin(); it != myList.end(); ++it){
		if(str.compare(*it) == 0){
			return i;
		}
		i++;
	}

	return -1;
}

int Utils::toInt(std::string str){
	unsigned int value = 0;
	for (unsigned int i = 0; i < str.length(); ++i){
		value = value * 10 + str.c_str()[i] - '0';
	}
	return value;
}

std::string Utils::getSubstring(std::string str, std::string start, std::string end){
	unsigned int pos, len;
	pos = str.find(start) + 1;
	len = str.find(end) - pos;
	if (pos != std::string::npos && len!=std::string::npos){
		return str.substr (pos, len);
	}
	return NULL;
}

std::string Utils::getSubstring(std::string str, int start, std::string end){
    unsigned int len;
	len = str.find(end);
	if (len!=std::string::npos){
		return str.substr (start, len);
	}
	return NULL;
}


std::string Utils::getSubstring(std::string str, std::string start){
    unsigned int pos;
	pos = str.find(start) + 1;
	if (pos != std::string::npos){
		return str.substr (pos);
	}
	return NULL;
}

std::string Utils::removeChar(std::string str, char ch){
	str.erase (std::remove(str.begin(), str.end(), ch), str.end());
	return str;
}

bool Utils::containsElement(std::list<std::string> list, std::string element){
	return std::find(list.begin(), list.end(), element) != list.end();
}


// src: http://stackoverflow.com/questions/997946/how-to-get-current-time-and-date-in-c
const std::string Utils::getCurrentDateTime(){
	time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d . %X", &tstruct);

    return buf;
}