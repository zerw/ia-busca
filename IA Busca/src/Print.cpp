#include "Print.h"

Print::Print()
{
    //ctor
}

Print::~Print()
{
    //dtor
}



void Print::printPaths(std::list<Path> paths){
	for (std::list<Path>::iterator it=paths.begin(); it != paths.end(); ++it){
		std::cout << "\t\t" << "Source Node:" << "\t\t" << it->getNodeSource().getName() << "\t" << (it->getNodeSource().isBegin() == true ? "Inicial" : "") << (it->getNodeSource().isEnd() == true? "Final" : "") << std::endl ;
		std::cout << "\t\t" << "Destination Node:" << "\t" << it->getNodeDest().getName() << "\t" << (it->getNodeDest().isBegin() == true? "Inicial" : "")  << (it->getNodeDest().isEnd() == true? "Final" : "") << std::endl ;
		std::cout << "\t\t" << "Cost:" << "\t\t\t" << it->getCost() << std::endl ;
		std::cout << "\t\t" << "__________________________" << std::endl ;
	}

}

void Print::printGraph(Graph graph){
	std::cout << "Graph" << std::endl ;
	std::cout << "\t" << "Path" << std::endl ;
	printPaths(graph.getPaths());
	std::cout << std::endl ;
	std::cout << "\t" << "Heuristic" << std::endl ;
	printPaths(graph.getHeuristics());

}

void Print::printList(std::list<std::string> list, std::string title){
	std::cout << title << std::endl ;
	for (std::list<std::string>::iterator it=list.begin(); it != list.end(); ++it){
		std::cout << ' ' << *it ;
	}
	std::cout << std::endl;
	std::cout << "____________________" <<std::endl;
	std::cout << std::endl;
}

void Print::printNodes(std::list<Node> nodes){
	std::cout << "Nodes" << std::endl ;
	for (std::list<Node>::iterator it=nodes.begin(); it != nodes.end(); ++it){
		std::cout << ' ' << it->getName() ;
	}
	std::cout << std::endl;
	std::cout << "____________________" <<std::endl;
	std::cout << std::endl;
}

void Print::printStack(std::stack<Node> nodes){
	std::cout << "Stack" << std::endl ;
	std::string str = "";
	while(nodes.size()){
		str = nodes.top().getName() + " " + str;
		nodes.pop();
	}
	std::cout << str << std::endl;
	std::cout << "____________________" <<std::endl;
	std::cout << std::endl;
}

void Print::printString(std::string str){
	std::cout << str << std::endl;
	std::cout << std::endl;
}

