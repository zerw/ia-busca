#include "Graph.h"

Graph::Graph(){}

Graph::~Graph(){}

std::list<Path> Graph::getPaths() {
	return paths;
}

std::list<Path> Graph::getPathsFromNode(Node node) {
	std::list<Path> pathsFromNode;

	for (std::list<Path>::iterator it=paths.begin(); it != paths.end(); ++it){
		if(it->getNodeSource().getName().compare(node.getName()) == 0){
			pathsFromNode.push_back(*it);
		}
	}

	return pathsFromNode;
}

std::list<Node> Graph::getChildsFromNode(Node node, int qntd){
	std::list<Node> childsFromNode;

	for (std::list<Path>::iterator it=paths.begin(); it != paths.end() && (qntd == 0 || childsFromNode.size() < (unsigned int)qntd); ++it){
		if(it->getNodeSource().getName().compare(node.getName()) == 0){
			childsFromNode.push_back(it->getNodeDest());
		}
	}

	return childsFromNode;
}

std::list<Path> Graph::getPathsFromRoot(){
	return getPathsFromNode(getRoot());
}

Node Graph::getRoot(){
	for (std::list<Path>::iterator it=paths.begin(); it != paths.end(); ++it){
		if(it->getNodeSource().isBegin()){
			return it->getNodeSource();
		}
	}
	return Node();
}

void Graph::addPath(Path path){
	paths.push_back(path);
}

std::list<Path> Graph::getHeuristics() {
	return heuristics;
}

Path Graph::getHeuristicFromNode(Node node) {
	Path heuristicFromNode;

	for (std::list<Path>::iterator it=heuristics.begin(); it != heuristics.end(); ++it){
		if(it->getNodeSource().getName().compare(node.getName()) == 0){
			heuristicFromNode = *it;
			break;
		}
	}

	return heuristicFromNode;
}

void Graph::addHeuristic(Path heuristic){
	heuristics.push_back(heuristic);
}

void Graph::reparse(std::string root, std::list<std::string> endStates){
	std::list<Path> pathsNew;
    std::list<Path> heuristicsNew;
    
    for (std::list<Path>::iterator it = this->paths.begin(); it != this->paths.end(); ++it){
		Node nodeDst = Node(it->getNodeDest().getName(), it->getNodeDest().getName().compare(root) == 0, Utils().containsElement(endStates, it->getNodeDest().getName()));
		Node nodeSrc = Node(it->getNodeSource().getName(), it->getNodeSource().getName().compare(root) == 0, Utils().containsElement(endStates, it->getNodeSource().getName()));
		double cost =  it->getCost();

		pathsNew.push_back(Path(nodeSrc, nodeDst, cost));
	}

	for (std::list<Path>::iterator it = this->heuristics.begin(); it != this->heuristics.end(); ++it){
		Node nodeDst = Node(it->getNodeDest().getName(), it->getNodeDest().getName().compare(root) == 0, Utils().containsElement(endStates, it->getNodeDest().getName()));
		Node nodeSrc = Node(it->getNodeSource().getName(), it->getNodeSource().getName().compare(root) == 0, Utils().containsElement(endStates, it->getNodeSource().getName()));
		double cost =  it->getCost();

		heuristicsNew.push_back(Path(nodeSrc, nodeDst, cost));
	}
	this->paths = pathsNew;
	this->heuristics = heuristicsNew;
}