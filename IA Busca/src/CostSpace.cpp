#include "CostSpace.h"

CostSpace::CostSpace()
{
    //ctor
}

CostSpace::~CostSpace()
{
    //dtor
}

CostSpace::CostSpace(int qntdElements, int sizeOfElements, int sizeInternalData){
	this->qntdElements = qntdElements;
	this->sizeOfElements =  sizeOfElements;
	this->sizeInternalData = sizeInternalData;
}

int CostSpace::getQntdElements(){
	return this->qntdElements;
}

int CostSpace::getSizeOfElements(){
	return this->sizeOfElements;
}

int CostSpace::getSizeInternalData(){
	return this->sizeInternalData;
}
