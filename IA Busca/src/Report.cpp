#include "Report.h"

Report::Report(std::string algorithmName){
    this->algorithmName = algorithmName;
    this->foundSolution = false;
}

Report::~Report(){}

void Report::addCostSpace(CostSpace borderCost, CostSpace visitingCost, CostSpace visitedCost){
	borderNodes.push_back(borderCost);
	visitedNodes.push_back(visitedCost);
	visitingNodes.push_back(visitingCost);
}

std::list<CostSpace> Report::getBordersNodes(){
	return this->borderNodes;
}

std::list<CostSpace> Report::getVisitedNodes(){
	return this->visitedNodes;
}

std::list<CostSpace> Report::getVisitingNodes(){
	return this->visitingNodes;
}

bool Report::getFoundSolution(){
	return this->foundSolution;
}

std::list<std::string> Report::getSolutionPath(){
	return this->solutionPath;
}

void Report::setSolutionPath(std::list<std::string> solutionPath){
	this->solutionPath = solutionPath;
	this->foundSolution = true;
}

std::string Report::getAlgorithmName(){
	return this->algorithmName;
}