#include "IO.h"

IO::IO(){}

IO::~IO(){}


Graph IO::readFile(){
	std::ifstream infile("entrada.txt");
	std::string line, initialState = "";
	std::list<std::string> endStates, log;
	Graph graph;
	int i = 0;

	if(infile.is_open()){
		while (!infile.eof()){
			std::getline(infile, line);
			i++;
			if (line.find("caminho(")!=std::string::npos){
				graph.addPath(findPathInLine(line, initialState, endStates));
			} else if (line.find("h(")!=std::string::npos && line.find("h(") == 0){
				graph.addHeuristic(findPathInLine(line, initialState, endStates));
			} else if (line.find("final(")!=std::string::npos){
				endStates.push_back(Utils().getSubstring(line, "(", ")"));
			} else if(line.find("inicial(")!=std::string::npos){
				if (initialState.empty()){
					initialState = Utils().getSubstring(line, "(", ")");
				} else{
					log.push_back("Warning. At line " + Utils().toString(i) + ", multiple root node. Since only one root is allowed, this root is ignored.");
				}
				
			} else if(line.length()){
				log.push_back("Warning. At line " + Utils().toString(i) +": \"" + line + "\" " + ", undefined data found. Line will be ignored.");
			}

		}
		log.push_back("");
		if(initialState.empty()){
			log.push_back("Total root: 0.");
		}else{
			log.push_back("Total root: 1.");
		}
		
		log.push_back("Total end states: " + Utils().toString(endStates.size()) + ".");
		log.push_back("Total paths: " + Utils().toString(graph.getPaths().size()) + ".");
		log.push_back("Total heuristics: " + Utils().toString(graph.getHeuristics().size()) + ".");
		this->writeReadLog(log);
		graph.reparse(initialState, endStates);
	} else{
		std::cout << "EUUUU" << std::endl;
		log.push_back("Error. Arquivo de entrada não encontrado.");
	}
	return graph;
}

Path IO::findPathInLine(std::string line, std::string initialState, std::list<std::string> endState){
	std::string substr;
	// pathStart
	substr = Utils().getSubstring(line, "(", ",");
	substr = Utils().removeChar(substr, ' ');
	// Node nodeSrc = Node(substr, substr.compare(initialState) == 0, substr.compare(endState) == 0);
	Node nodeSrc = Node(substr, substr.compare(initialState) == 0, Utils().containsElement(endState, substr));

	// pathEnd
	line = Utils().getSubstring(line, ",");
	substr = Utils().getSubstring(line, 0, ",");
	substr = Utils().removeChar(substr, ' ');
	// Node nodeDst = Node(substr, substr.compare(initialState) == 0, substr.compare(endState) == 0);
	Node nodeDst = Node(substr, substr.compare(initialState) == 0, Utils().containsElement(endState, substr));

	// cost
	line = Utils().getSubstring(line, ",");
	substr = Utils().getSubstring(line, 0, ")");
	substr = Utils().removeChar(substr, ' ');
	double cost =  Utils().toInt(substr);

	return Path(nodeSrc, nodeDst, cost);
}

void IO::writeReport(std::list<Report> reports){
	std::ofstream file;

	file.open("report.chris", std::ofstream::out | std::ofstream::trunc);

	time_t t = time(0);   // get time now
	struct tm * now = localtime( & t );

	file << "Report for searches algorithm" << std::endl;
	file << std::endl;
	file << "Date:" << (now->tm_year + 1900) << '-' << (now->tm_mon + 1) << '-' <<  now->tm_mday << std::endl;
	file << std::endl;
	for (std::list<Report>::iterator it=reports.begin(); it != reports.end(); ++it){
		file << "________________________" << std::endl;
		file << std::endl;
		file << it->getAlgorithmName()  << std::endl;
		file << std::endl;
		if (it->getFoundSolution()){
			file << "Solution found:";
			std::list<std::string> solutionPath = it->getSolutionPath();
			std::list<std::string>::iterator itSP = solutionPath.begin();
			while ( itSP != solutionPath.end()){
				file << ' ' << *itSP ;
				itSP++;
			}
		} else{
			file << "Solution not found" << std::endl;
		}
		file << std::endl;
		file << std::endl;
		file << "Space cost " << std::endl;
		file << "----------------------------------------------------------------------------------------------------" << std::endl;
		file << "\tType" << "\t\t|\tQntd Nodes" << "\t|\tSize of Nodes" << "\t|\t Size of Nodes + Size of Internal Structure" << std::endl;
		file << "----------------------------------------------------------------------------------------------------" << std::endl;
		std::list<CostSpace> borderNodes = it->getBordersNodes();
    	std::list<CostSpace> visitedNodes = it->getVisitedNodes();
    	std::list<CostSpace> visitingNodes = it->getVisitingNodes();

    	std::list<CostSpace>::iterator itBD = borderNodes.begin();
    	std::list<CostSpace>::iterator itVD = visitedNodes.begin();
    	std::list<CostSpace>::iterator itVN = visitingNodes.begin();

    	int i = 1;
		while (itBD != borderNodes.end() ){
			file << i++ << "\tBorder" << "\t\t|\t\t" << itBD->getQntdElements() << "\t\t|\t\t" << itBD->getSizeOfElements() << "\t\t\t|\t\t\t\t\t" << (itBD->getSizeOfElements() + itBD->getSizeInternalData())  << std::endl;
			file << "\tVisited" << "\t\t|\t\t" << itVD->getQntdElements() << "\t\t|\t\t" << itVD->getSizeOfElements() << "\t\t\t|\t\t\t\t\t" << (itVD->getSizeOfElements() + itVD->getSizeInternalData())  << std::endl;
			file << "\tVisiting" << "\t|\t\t" << itVN->getQntdElements() << "\t\t|\t\t" << itVN->getSizeOfElements() << "\t\t\t|\t\t\t\t\t" << (itVN->getSizeOfElements() + itVN->getSizeInternalData())  << std::endl;
			file << "\tTotal" << "\t\t|\t\t" << itBD->getQntdElements() + itVD->getQntdElements() + itVN->getQntdElements() << "\t\t|\t\t" << itBD->getSizeOfElements() + itVD->getSizeOfElements() + itVN->getSizeOfElements() << "\t\t\t|\t\t\t\t\t" << (itBD->getSizeOfElements() + itBD->getSizeInternalData()) + (itVD->getSizeOfElements() + itVD->getSizeInternalData())  + (itVN->getSizeOfElements() + itVN->getSizeInternalData()) << std::endl;
			itBD++;
			itVD++;
			itVN++;
			file << "----------------------------------------------------------------------------------------------------" << std::endl;
		}
	}

  	file.close();
}

void IO::writeReadLog(std::list<std::string> log){
	std::ofstream file;
	int warning = 0;
	int error = 0;

	file.open("readGraph.log", std::ofstream::out | std::ofstream::app);

	// time_t t = time(0);   // get time now
	// struct tm * now = localtime( & t );

	// file << "Date:" << (now->tm_year + 1900) << '-' << (now->tm_mon + 1) << '-' <<  now->tm_mday << std::endl;
	file << "Date: " << Utils().getCurrentDateTime() << "." << std::endl;
	file << std::endl;
	// file << "Total Warnings: " << log.size() - (log.size() >=5 ? 5 : 0) << ".";
	file << std::endl;
	for (std::list<std::string>::iterator it=log.begin(); it != log.end(); ++it){
		if(it->find("Warning")!=std::string::npos){
			warning++;
		} else if(it->find("Error")!=std::string::npos){
			error++;
		}
		file << *it << std::endl;
	}
	file << "Total Warnings: " << warning << "." << std::endl;
	file << "Total Error: " << error << "." << std::endl;
	file << "=======================================" << std::endl;
	file.close();
}
