#include "Search.h"

Search::Search(){}

Search::~Search(){}

std::stack<Node> Search::insertIntoStack (std::stack<Node> dest, std::list<Node> src){
	for (std::list<Node>::reverse_iterator it=src.rbegin(); it != src.rend(); ++it){
		dest.push(*it);
	}
	return dest;
}

std::list<std::string> Search::insertIntoList (std::list<Node> src){
	std::list<std::string> finalList;
	for (std::list<Node>::iterator it=src.begin(); it != src.end(); ++it){
		finalList.push_back((*it).getName());
		// dest.push_back(*it);
	}
	return finalList;
}

Node Search::getBestNodeFromList (Graph graph, std::list<Node> dest){
	Node bestNode = dest.front();
	for (std::list<Node>::iterator it=dest.begin(); it != dest.end(); ++it){
		// std::cout << (*it).getName() << " com custo: " << graph.getHeuristicFromNode(*it).getCost() << "---- \n";
		if( graph.getHeuristicFromNode(*it).getCost() < graph.getHeuristicFromNode(bestNode).getCost() )
			bestNode = *it;
	}
	return bestNode;
}

std::list<Node> Search::removeVisitedNodes(std::list<Node> child, std::list<std::string> visiting, std::list<std::string> visited){
	int i = 0;
	for (std::list<Node>::iterator it=child.begin(); it != child.end(); ++it){
		i++;
		if(
			Utils().containsElement(visiting, it->getName()) ||
			Utils().containsElement(visited, it->getName())
		){
			it = child.erase(it);
			it--;
		}
	}
	return child;
}

Report Search::greedySearch(Graph graph){
	Node no;
	std::stack<Node> nodes;
	std::list<std::string> visitingNodes;
	std::list<std::string> visitedNodes;
	int it, sizeofVisited, sizeofVisiting, sizeofStack;
	Report report = Report("Greedy Search");

	it = sizeofStack = sizeofVisiting = sizeofVisited = 0;
	nodes.push(graph.getRoot());
	std::list<Node> childNodes;
	// visitingNodes.push_back(nodes.top().getName());
	CostSpace visitedCost, borderCost(0,0,0);
	CostSpace visitingCost(0,0,0);

	while(nodes.size()){

		// CostSpace visitedCost = CostSpace(visitedNodes.size(), sizeof(std::string) * visitedNodes.size(), sizeof(visitedNodes));
		// CostSpace borderCost = CostSpace(nodes.size(), sizeof(std::string) * nodes.size(), sizeof(nodes));
		// CostSpace visitingCost = CostSpace(visitingNodes.size(), sizeof(std::string) * visitingNodes.size(), sizeof(visitingNodes));
		// report.addCostSpace(borderCost, visitingCost, visitedCost);

		std::cout << "# - De: " << nodes.top().getName() << std::endl;

		// se eh final, insere na lista de visitados (formando o path)
		if (nodes.top().isEnd()){
			visitedNodes.push_back(nodes.top().getName());
			// report.addCostSpace(borderCost, visitingCost, visitedCost);
			report.setSolutionPath(visitedNodes);
			break;
		} else{
			visitedNodes.push_back(nodes.top().getName());

			childNodes = graph.getChildsFromNode(nodes.top(), 0 );
			childNodes = removeVisitedNodes(childNodes, visitingNodes, visitedNodes);

			// for (std::list<Node>::iterator at=childNodes.begin();
			// 		at != childNodes.end(); ++at)
			// {
			// 	std::cout << (*at).getName() << " | ";
			// }

			// std::cout <<std::endl;

			nodes.pop();
			if(childNodes.size()){
				no = getBestNodeFromList(graph,childNodes);
				std::cout << "# - Para: " << no.getName() << std::endl;
				nodes.push(no);
				// visitingNodes.push_back(nodes.top().getName());
				std::cout << "# have child #\n";
			} else{
				// visitingNodes.pop_back();
			}
			std::cout << "#=========================#\n";

		}
		visitedCost = CostSpace(visitedNodes.size(), sizeof(std::string) * visitedNodes.size(), sizeof(visitedNodes));
		// borderCost = CostSpace(nodes.size(), sizeof(std::string) * nodes.size(), sizeof(nodes));
		// visitingCost = CostSpace(visitingNodes.size(), sizeof(std::string) * visitingNodes.size(), sizeof(visitingNodes));
		report.addCostSpace(borderCost, visitingCost, visitedCost);

		it++;
		std::cout  << std::endl;
	}
	visitedCost = CostSpace(visitedNodes.size(), sizeof(std::string) * visitedNodes.size(), sizeof(visitedNodes));
	// borderCost = CostSpace(nodes.size(), sizeof(std::string) * nodes.size(), sizeof(nodes));
	// visitingCost = CostSpace(visitingNodes.size(), sizeof(std::string) * visitingNodes.size(), sizeof(visitingNodes));
	report.addCostSpace(borderCost, visitingCost, visitedCost);
	return report;
}


Report Search::depthFirstSearch(Graph graph, bool isBacktrack){
	std::stack<Node> nodes;
	std::list<std::string> visitingNodes;
	std::list<std::string> visitedNodes;
	int it, sizeofVisited, sizeofVisiting, sizeofStack;
	std::string name;

	if(isBacktrack){
		name = "Deep-First Search with Backtrack";
	} else{
		name = "Deep-First Search without Backtrack";
	}
	Report report = Report(name);
	CostSpace visitedCost, visitingCost, borderCost;

	it = sizeofStack = sizeofVisiting = sizeofVisited = 0;
	nodes.push(graph.getRoot());
	// visitingNodes.push_back(nodes.top().getName());
	std::cout << name << std::endl << std::endl;
	while(nodes.size()){
		Print().printList(visitingNodes, "Visiting Nodes");
		Print().printList(visitedNodes, "Visited Nodes");
		Print().printStack(nodes);
		if (nodes.top().isEnd()){
			if (nodes.top().getName().compare(visitingNodes.back())!=0){
				visitingNodes.push_back(nodes.top().getName());
			}
			// report.addCostSpace(borderCost, visitingCost, visitedCost);
			report.setSolutionPath(visitingNodes);
			break;
		} else{
			if (!Utils().containsElement(visitingNodes, nodes.top().getName())){
				visitingNodes.push_back(nodes.top().getName());
			} //else{
				std::list<Node> childNodes = graph.getChildsFromNode(nodes.top(), (isBacktrack == true ? 0 : 1) );
				childNodes = removeVisitedNodes(childNodes, visitingNodes, visitedNodes);
				if(childNodes.size()){
					if(isBacktrack){
						nodes = insertIntoStack(nodes, childNodes);
					} else{
						nodes.push(childNodes.front());
					}
					// visitingNodes.push_back(nodes.top().getName());
				} else if(!isBacktrack){
					break;
				}else{

					visitedNodes.push_back(visitingNodes.back());
					visitingNodes.pop_back();
					nodes.pop();

				}
			// }
		}
		visitedCost = CostSpace(visitedNodes.size(), sizeof(std::string) * visitedNodes.size(), sizeof(visitedNodes));
		borderCost = CostSpace(nodes.size() - visitingNodes.size(), sizeof(std::string) * nodes.size() - sizeof(std::string) * visitingNodes.size(), sizeof(nodes) - sizeof(visitingNodes));
		visitingCost = CostSpace(visitingNodes.size(), sizeof(std::string) * visitingNodes.size(), sizeof(visitingNodes));
		report.addCostSpace(borderCost, visitingCost, visitedCost);

		it++;
		 std::cout << "================================" << std::endl;
	}
	visitedCost = CostSpace(visitedNodes.size(), sizeof(std::string) * visitedNodes.size(), sizeof(visitedNodes));
	borderCost = CostSpace(nodes.size() - visitingNodes.size(), sizeof(std::string) * nodes.size() - sizeof(std::string) * visitingNodes.size(), sizeof(nodes) - sizeof(visitingNodes));
	visitingCost = CostSpace(visitingNodes.size(), sizeof(std::string) * visitingNodes.size(), sizeof(visitingNodes));
	report.addCostSpace(borderCost, visitingCost, visitedCost);
	return report;
}
