#include "Node.h"

Node::Node(std::string name, bool begin, bool end){
	this->name = name;
	this->begin = begin;
	this->end = end;
}

Node::~Node(){}

Node::Node(){}


void Node::setName (std::string name){
	this->name = name;
}

void Node::setBegin (bool begin){
	this->begin = begin;
}

void Node::setEnd (bool end){
	this->end = end;
}


std::string Node::getName(){
	return this->name;
}

bool Node::isBegin(){
	return this->begin;
}

bool Node::isEnd(){
	return this->end;
}