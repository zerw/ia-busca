#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <list>
#include <algorithm>
#include <sstream>
#include <time.h>

class Utils
{
    public:
        Utils();
        virtual ~Utils();
        int find(std::list<std::string>, std::string);
    	int toInt(std::string);
        std::string toString(int);
    	std::string getSubstring(std::string, std::string, std::string);
    	std::string getSubstring(std::string, int, std::string);
    	std::string getSubstring(std::string, std::string);
    	std::string removeChar(std::string, char);
        bool containsElement(std::list<std::string>, std::string);
        const std::string getCurrentDateTime();
    protected:

    private:
};

#endif // UTILS_H
