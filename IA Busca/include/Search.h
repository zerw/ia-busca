#ifndef SEARCH_H
#define SEARCH_H

#include <list>
#include <stack>
#include <string>
#include <algorithm>

#include <iostream>

#include "Node.h"
#include "Path.h"
#include "Graph.h"
#include "Utils.h"
#include "Print.h"
#include "Report.h"
#include "CostSpace.h"


class Search
{
    public:
        Search();
        virtual ~Search();

        Report depthFirstSearch(Graph, bool);
        Report greedySearch(Graph);
    protected:

    private:
    	std::stack<Node> insertIntoStack (std::stack<Node>, std::list<Node>);
        std::list<std::string> insertIntoList (std::list<Node>);
    	std::list<Node> removeVisitedNodes(std::list<Node>, std::list<std::string>, std::list<std::string>);
        Node getBestNodeFromList (Graph, std::list<Node>);
};

#endif // SEARCH_H
