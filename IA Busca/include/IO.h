#ifndef IO_H
#define IO_H

#include <fstream>
#include <iostream>
#include <string>
#include <list>
#include <algorithm>
#include <sstream>
#include <ctime>

#include "Utils.h"
#include "Node.h"
#include "Graph.h"
#include "Report.h"
#include "CostSpace.h"


class IO{
    public:
        IO();
        virtual ~IO();

        Graph readFile();
        void writeReport(std::list<Report>);
        
    protected:

    private:
    	Path findPathInLine(std::string, std::string, std::list<std::string>);
        void writeReadLog(std::list<std::string>);
    	
};

#endif // IO_H
