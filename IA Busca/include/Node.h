#ifndef NODE_H
#define NODE_H

#include <string>

class Node{
    public:
        Node(std::string, bool, bool);
        Node();
        virtual ~Node();
        void setName (std::string name);
    	void setBegin (bool begin);
    	void setEnd (bool end);

    	std::string getName();
    	bool isBegin();
    	bool isEnd();
    protected:

    private:
    	std::string name;
    	bool begin;
    	bool end;

};

#endif // NODE_H
