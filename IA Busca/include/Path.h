#ifndef PATH_H
#define PATH_H

#include "Node.h"

class Path
{
    public:
        Path(Node, Node, int);
        Path();
        virtual ~Path();

        Node getNodeSource() { return nodeSource; }
        void setNodeSource(Node val) { nodeSource = val; }
        Node getNodeDest() { return nodeDest; }
        void setNodeDest(Node val) { nodeDest = val; }
        int getCost() { return cost; }
        void setCost(int val) { cost = val; }
    protected:

    private:
        Node nodeSource;
        Node nodeDest;
        int cost;
};

#endif // PATH_H
