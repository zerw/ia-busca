#ifndef REPORT_H
#define REPORT_H

#include <list>
#include <string>

#include "CostSpace.h"

class Report{
    public:
        Report(std::string);
        virtual ~Report();

        std::list<CostSpace> getBordersNodes();
        std::list<CostSpace> getVisitedNodes();
        std::list<CostSpace> getVisitingNodes();

        void addCostSpace(CostSpace, CostSpace, CostSpace);
        bool getFoundSolution();
        std::list<std::string> getSolutionPath();
        std::string getAlgorithmName();
        void setSolutionPath(std::list<std::string>);
    protected:

    private:
    	std::list<CostSpace> borderNodes;
    	std::list<CostSpace> visitedNodes;
    	std::list<CostSpace> visitingNodes;
        std::string algorithmName;
        std::list<std::string> solutionPath;
        bool foundSolution;

};

#endif // REPORT_H
