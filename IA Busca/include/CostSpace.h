#ifndef COSTSPACE_H
#define COSTSPACE_H


class CostSpace
{
    public:
    	CostSpace();
        CostSpace(int, int, int);
        virtual ~CostSpace();
        int getQntdElements();
    	int getSizeOfElements();
    	int getSizeInternalData();
    protected:

    private:
    	int qntdElements;
    	int sizeOfElements;
    	int sizeInternalData;
};

#endif // COSTSPACE_H
