#ifndef PRINT_H
#define PRINT_H

#include <list>
#include <string>
#include <stack>
#include <iostream>

#include "Node.h"
#include "Path.h"
#include "Graph.h"

class Print{
    public:
        Print();
        virtual ~Print();

        void printPaths(std::list<Path>);
        void printNodes(std::list<Node>);
        void printGraph(Graph);
        void printList(std::list<std::string>, std::string);
        void printStack(std::stack<Node>);
        void printString(std::string);


    protected:

    private:
};

#endif // PRINT_H
