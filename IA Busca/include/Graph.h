#ifndef GRAPH_H
#define GRAPH_H

#include <list>
#include <string>

#include "Node.h"
#include "Path.h"
#include "Utils.h"


class Graph{
    public:
        Graph();
        virtual ~Graph();

        std::list<Path> getPaths();
        std::list<Path> getPathsFromNode(Node);
        std::list<Path> getPathsFromRoot();
        std::list<Node> getChildsFromNode(Node, int);
        Node getRoot();
        void addPath(Path val);
        std::list<Path> getHeuristics();
        Path getHeuristicFromNode(Node);
        void addHeuristic(Path val);
        void reparse(std::string, std::list<std::string>);

    protected:

    private:
        std::list<Path> paths;
        std::list<Path> heuristics;
};

#endif // GRAPH_H
